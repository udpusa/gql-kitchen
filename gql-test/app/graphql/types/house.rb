class Types::House < GraphQL::Schema::Object
  field :id, Int, null: true, method: :id
  field :kitchen, Types::Kitchen, null: true, method: :kitchen
end
