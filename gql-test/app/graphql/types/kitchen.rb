class Types::Kitchen < GraphQL::Schema::Object
	Types::House
  field :other_than_house, Int, null: true, method: :other_than_house

  field :house, Types::House, null: true, method: :house
  def house
  	{
  		id: 1,
  		kitchen: {
  			other_than_house: 2,
  			house: nil
  		}
  	}
  end
end
